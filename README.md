# Greedy Matroids

Greedy gray codes for generating the bases of a matroid in SageMath.

## Getting started

The file greedy_bases.sage contains two implementations of the basis generation algorithm developed in the paper "All your bases are belong to us".
The function "greedy_base_closest" implements the closest tiebreaker rule via an active vector, while "greedy_base_furthest" implements the furthest tiebreaker rule via a stack. Both functions have as inputs the matroid M and the initial basis B. 